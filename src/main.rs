extern crate config;
mod lib;

use lib::OwmResponse;
use config::{Config, File};
use chrono::{NaiveDateTime, NaiveTime};
//use chrono::format::StrftimeItems;
use std::path::Path;
use dirs::config_dir;


#[derive(Debug)]
struct WaybarWeather {
    temp: f64,
    icon: String,
    condition: String,
    description: String,
    clouds: i64,
    sunrise: NaiveTime,
    sunset: NaiveTime,
}

impl WaybarWeather {
    fn new(resp: &OwmResponse) -> WaybarWeather {
        WaybarWeather {
            temp: resp.main.temp,
            icon: resp.weather[0].icon.clone(),
            condition: resp.weather[0].main.clone(),
            description: resp.weather[0].description.clone(),
            clouds: resp.clouds.all,
            sunrise: NaiveDateTime::from_timestamp(resp.sys.sunrise, 0).time(),
            sunset: NaiveDateTime::from_timestamp(resp.sys.sunset, 0).time()
        }
    }

    fn to_string(self) -> String {
        //let fmt = StrftimeItems::new("%H:%M");

        format!("{}, {}°C, {}% clouds", //, sunrise: {}, sunset: {}",
                self.description,
                self.temp,
                self.clouds,
                //self.sunrise.format_with_items(fmt.clone()),
                //self.sunset.format_with_items(fmt.clone()),
        )}

}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    // ======================================================================
    // read config file
    let mut settings = Config::default();

    let con_dir = config_dir().unwrap();
    let config_dir = Path::new(con_dir.to_str().unwrap());
    let settings_file = config_dir.join("waybar").join("modules").join("Settings.json");
    settings.merge(File::from(settings_file)).unwrap();

    let owmresp = OwmResponse::new(settings);
    let ww = WaybarWeather::new(&owmresp);
    println!("{}", ww.to_string());
    Ok(())
}
