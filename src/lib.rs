extern crate config;

use serde::Deserialize;
use config::Config;
use std::collections::HashMap;


#[derive(Deserialize, Debug)]
pub struct Coord {
    pub lon: f64,
    pub lat: f64
}

#[derive(Deserialize, Debug)]
pub struct Weather {
    pub id: i64,
    pub main: String,
    pub description: String,
    pub icon: String
}

#[derive(Deserialize, Debug)]
pub struct Main {
    pub temp: f64,
    pub feels_like: f64,
    pub temp_min: f64,
    pub temp_max: f64,
    pub pressure: f64,
    pub humidity: f64,
}

#[derive(Deserialize, Debug)]
pub struct Wind {
    pub speed: f64,
    pub deg: i64
}

#[derive(Deserialize, Debug)]
pub struct Clouds {
    pub all: i64
}

#[derive(Deserialize, Debug)]
pub struct Sys {
    pub country: String,
    pub sunrise: i64,
    pub sunset: i64,
}

#[derive(Deserialize, Debug)]
pub struct OwmResponse {
    pub coord: Coord,
    pub weather: Vec<Weather>,
    pub base: String,
    pub main: Main,
    pub visibility: i64,
    pub wind: Wind,
    pub clouds: Clouds,
    pub dt: i64,
    pub sys: Sys,
    pub timezone: i64,
    pub name: String,
}

impl OwmResponse {
    pub fn new(settings: Config) -> OwmResponse {
        let settings = settings.try_into::<HashMap<String, String>>().unwrap();
        // ======================================================================
        // parse city name
        let city_name = settings.get("city_name").ok_or("London").unwrap();

        // parse api key
        let api_key = settings.get("api_key").expect("No api key provided!");

        // parse units
        let units = if let Some(units) = settings.get("units") {
            match units.as_str() {
                "imperial"|"metric" => units.to_owned(),
                _          => {
                    eprintln!("Invalid setting for units: {}", units);
                    "metric".to_owned()
                }
            }
        } else {
            "metric".to_owned()
        };

        let url = format!("https://api.openweathermap.org/data/2.5/weather?q={}&appid={}&units={}", city_name, api_key, units);
        let resp = reqwest::blocking::get(&url).unwrap().text().unwrap();
        serde_json::from_str(&resp).unwrap()
    }
}
